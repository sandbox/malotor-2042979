<?php
/**
* @file
* Author hcar module
* This module generates a block with node´s author hcard. */

require_once('author_profile_functions.inc.php');

/**
* Implements hook_block_info().
*/
function author_profile_block_info() { 
  //Declaración del bloque 
  $blocks['author_profile'] = array(
    'info' => t('Author hcard'),
    'cache' => DRUPAL_NO_CACHE,
  );
  /*
  $blocks['author_social_entries'] = array(
    'info' => t('Author´s entries'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['author_social_links'] = array(
    'info' => t('Author social link'),
    'cache' => DRUPAL_NO_CACHE,
  );
  */
  $blocks['author_social_widgets'] = array(
    'info' => t('Author social widgets'),
    'cache' => DRUPAL_NO_CACHE,
  );
  return $blocks; 
}

/**
* Implements hook_permission().
*/
function author_profile_permission() { 
  return array(
    'access to author profile' => array(
      'title' => t('Access to the author´s profile page'), 
      'description' => t('Allows access to the author´s profile pages'),
    ),
  ); 
}


/**
 * Implements hook_menu().
*/

function author_profile_menu() { 

  //Profile url
  //Check if we have activated the profile pages
  if (variable_get('author_profile_activate_profile')){
    $items['about/%'] = array(
      'title' => 'Author´s profile',
      'description' => 'Author´s profile',
      'page callback' => 'author_profile_profile_page',
      'page arguments' => array(1),
      'access callback' => 'user_access',
      'access arguments' => array('access to author profile'),
      'type' => MENU_CALLBACK,
    );
  }
  
  
  
  //Administration url
  $items['admin/config/people/author_profile'] = array(
    'title' => 'Author profile setting',
    'description' => 'Settings for module Author profile',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('author_profile_admin_settings'), 
    'access callback' => 'user_access',
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'author_profile.admin.inc',
  );
  
  return $items; 
}

/**
* Implements hook_block_view().
*/
function author_profile_block_view($delta = '') {
  //Contenido del bloque 'welcome'definido en hook_block_info(). 
  $block = array();
  
  
  //Get the current node id
  if (arg(0) == 'node' && is_numeric(arg(1))) {
    
    $nid = arg(1);
    
    //Get the node
    $node=node_load($nid);
    //Get the author node
    $author = user_load($node->uid);
   
    //If author don´t exsit don´t show the block
    if ($author->uid==0) {
      drupal_set_message(t('This node doesn´t have an author'), 'status');
      return null;
    }
    else {
      switch ($delta) {
        case 'author_profile':
        
          $block['subject'] = t('Author hcard'); 
          $block['content'] = theme('author_profile', 
              array(
                'user_name' => $author->name,
                'name' => _author_profile_get_full_name($author), 
                'email' => $author->mail, 
                'image' => _author_profile_get_image($author),
                'desc' => $author->user_description ? $author->user_description['und'][0]['summary'] : '',
                'twitter' => 'https://twitter.com/' . $author->user_twitter ? $author->user_twitter['und'][0]['safe_value'] : '',
                'facebook' => $author->user_facebook ? $author->user_facebook['und'][0]['safe_value'] : '',
                'linkedin' => $author->user_linkedin ? $author->user_linkedin['und'][0]['safe_value'] : '',
                'github' => $author->user_github ? $author->user_github['und'][0]['safe_value'] : '',
                'google' => $author->user_google ? $author->user_google['und'][0]['safe_value'] : '',
                'uid' => $author->uid,
              )
          );
           
        break;
         
        //Content for bloc Author widgets
        case 'author_social_widgets';
          $block['subject'] = t('Author social widgets'); 
          $block['content'] = theme('author_profile_social_widgets', 
              array(       
                'twitter_widget' => _author_profile_twitter_widget($author),
                'google_widget' => _author_profile_google_widget($author),
              )
          );
        
        break;
        //Content for bloc Author entries
        case 'author_social_entries';
          //Podemos cargar los nodos de varias maneras
		   
          //FORMA 1         
          $nodes = db_query("SELECT nid, title FROM {node} WHERE uid = :uid AND type = 'blog' order by created DESC", array(':uid' => $author->uid));
		  
    		  /* //FORMA 2 - funcion node_load_multiple 
    		  $nodes = node_load_multiple(array(), array(
                'type' => 'blog',
                'uid' => $author->uid
              ), FALSE);
              */
    		  
    		  //FORMA 3
    		  /*
    		  $query = db_select('node', 'n')
    		  ->fields('n', array('nid', 'title')) 
    		  ->condition('uid', $author->uid, '=');
    		  $query->orderBy('created', 'DESC');
    		  //Ejecuta la sentencia
    		  $nodes = $query->execute();
    		  */
		  
          $items=array();
          
          if ($nodes)
          foreach($nodes as $item) {
            $foo['data']='<a href="'.  url("node/" . $item->nid) .'">' . $item->title . "</a>";
            $foo['id']=$item->nid;
            $items[]=$foo;
            unset($foo);  
          }
          
          $block['content'] = theme_item_list(array(
    			  'items' => $items, 
    			  'title' => t("Author last entries"),
    			  'type' => 'ul',
    			  'attributes' => array('class' => 'my-list-class')
            )
          );
        break;
        
         
      }
      return $block; 
    }
    
  }

  
  
}

/** 
* Implements hook_help().
*/
function author_profile_help($path, $arg) {
  switch ($path) {
    // Ayuda general para el módulo First example 
    case 'admin/help#author_profile':
      return '<p>' . t('This module shows a welcome message into ablock.') . '</p>'; 
    // Ayuda particular que se mostrará en el área deadministración de bloques
    case 'admin/structure/block':
    return '<p>' . t('Use the block Welcome block to show a welcome message.') . '</p>';
  }
}

/** 
* Implements hook_theme().
*/
function author_profile_theme() {
  return array(
    'author_profile' => array(
      'template' => 'author_profile_hcard',
      'path' => drupal_get_path('module', 'author_profile').'/templates',
      'variables' => array(
        'name' => NULL, 
        'email' => NULL, 
        'image' => NULL,         ),
    ),
    'author_profile_profile' => array(
      'template' => 'author_profile_profile',
      'path' => drupal_get_path('module', 'author_profile').'/templates',
      'variables' => array(
        'name' => NULL, 
        'email' => NULL, 
        'image' => NULL,         ),
    ),
    'author_profile_social_links' => array(
      'template' => 'author_profile_social_links',
      'path' => drupal_get_path('module', 'author_profile').'/templates',
      'variables' => array(),
    ),
    'author_profile_social_widgets' => array(
      'template' => 'author_profile_social_widgets',
      'path' => drupal_get_path('module', 'author_profile').'/templates',
      'variables' => array(),
    ),
  );
}
/** 
* Implements hook_theme().
*/
function author_profile_form_alter(&$form, &$form_state, $form_id) {
    switch ($form_id) {
    case 'user_profile_form':
     $form['author_profile_user_info'] = array(
        '#type' => 'fieldset',
        '#title' => t('Profile info'), 
        '#collapsible' => TRUE, 
        '#collapsed' => FALSE,
      );
      
      $form['author_profile_accounts'] = array(
        '#type' => 'fieldset',
        '#title' => t('Accounts'), 
        '#collapsible' => TRUE, 
        '#collapsed' => FALSE,
      );
      
      $form['author_profile_social_links'] = array(
        '#type' => 'fieldset',
        '#title' => t('Social links'), 
        '#collapsible' => TRUE, 
        '#collapsed' => FALSE,
      );


      
      $form['author_profile_user_info']['user_name']=$form['user_name'];
      unset($form['user_name']);
      $form['author_profile_user_info']['user_surname']=$form['user_surname'];
      unset($form['user_surname']);
      $form['author_profile_user_info']['user_description']=$form['user_description'];
      unset($form['user_description']);
     
      $form['author_profile_social_links']['user_linkedin']=$form['user_linkedin'];
      unset($form['user_linkedin']);
      $form['author_profile_social_links']['user_github']=$form['user_github'];
      unset($form['user_github']);
      $form['author_profile_social_links']['user_facebook']=$form['user_facebook'];
      unset($form['user_facebook']);
      $form['author_profile_social_links']['user_google']=$form['user_google'];
      unset($form['user_google']);
      
      
      $form['author_profile_accounts']['user_twitter']=$form['user_twitter'];
      unset($form['user_twitter']);

      
      //usort($a, "drupal_sort_weight");
     
    break;
  }
}
/**
 * Implements hook_node_view().
 */
function author_profile_node_view($node, $view_mode, $langcode) {
 
  $author = user_load($node->uid);
  $node->name = _author_profile_get_name($author);
  
  return;
}

